
import os.path, tempfile, zipfile, magic, time
from distutils import dir_util, file_util

from PyQt4 import QtCore, QtGui

from pypapyr.main_ui import Ui_MainWindow
from pypapyr.epub import Epub

class MainWindow(QtGui.QMainWindow):
    current_book = None
    books = {}
    
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        
        # Initialize temp dir for epubs
        self.cache_path = os.path.dirname(__file__) + "/../cache/"
        self.library_path = os.path.dirname(__file__) + "/../Books/"
        
        # Load interface
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        # Load library
        self.loadLibrary()
        
        # Load reader
        reader_url = os.path.join("html", "reader.html")
        self.ui.webView.setUrl(QtCore.QUrl(reader_url))
        
        # Signals and slots
        self.ui.libraryView.itemDoubleClicked.connect(self.bookItemClicked)
        #self.ui.webView.loadFinished.connect(self.reloadBook)
        #self.ui.actionRefresh.activated.connect(self.reloadBook)
        
        
    def loadLibrary(self):
        self.books = {}
        for a, b, c in os.walk(self.library_path):
            for d in c:
                filepath = os.path.join(a, d)
                with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as m: 
                    mimetype = m.id_filename(filepath)

                epubobj = Epub(filepath, self.cache_path)
                if mimetype == "application/epub+zip":
                    self.ui.libraryView.addThumb(epubobj.get_cover_image(), filepath)
                    self.books[filepath] = epubobj
    
    def bookItemClicked(self, item):
        filename = item.getPath()
        self.loadBook(filename)
        
    def loadBook(self, filename):
        # Set current book
        self.current_book = self.books[filename]
        
        # change tab
        self.ui.tabWidget.setCurrentIndex(1)
        self.ui.webView.reload()
        
        def load():
            # load in readium
            opf_file = self.current_book.opf_path
            frame = self.ui.webView.page().mainFrame()
            frame.evaluateJavaScript('RJSApp.loadBook("{0}")'.format(opf_file))
            return True
        
        QtCore.QTimer.singleShot(1000, load)
        
        
    def resizeEvent(self, event):
        frame = self.ui.webView.page().mainFrame()
        frame.evaluateJavaScript('RJSApp.setModuleContainerHeight();');
        #time.sleep(1);
        #frame.evaluateJavaScript('RJSApp.epubViewer.openPageRight();');
        
        