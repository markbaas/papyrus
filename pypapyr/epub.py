import re, os, zipfile
from distutils import dir_util

from lxml import etree

class Epub():
    def __init__(self, filename, cache_path):
        self.filename = filename
        self.cache_path = cache_path
        self._metadata = {}
        
        # Create directory for zip
        path = os.path.join(self.cache_path, os.path.basename(filename))
        dir_util.mkpath(path)
        
        # extract content
        with zipfile.ZipFile(filename, 'r') as epubzip:
            epubzip.extractall(path)
            
        self.opf_path = self._get_opf_path()
        self._get_metadata()
     
    def _get_opf_path(self):
        # get opf path
        f = open(os.path.join(self.cache_path, os.path.basename(self.filename), "META-INF", "container.xml"))
        metainfo = f.read()
        f.close()
        regex = re.compile("<rootfile full-path=\"([\w\/\.]+)\".+?>")
        result = regex.findall(metainfo)
        return os.path.join(self.cache_path, os.path.basename(self.filename), result[0].replace("/", os.sep))
    
    def _get_metadata(self):
        # namespaces for xml
        ns = {
            'n':'urn:oasis:names:tc:opendocument:xmlns:container',
            'pkg':'http://www.idpf.org/2007/opf',
            'dc':'http://purl.org/dc/elements/1.1/',
        }
        
        # parse file
        tree = etree.parse(self.opf_path)
        
        # get all metadata
        self._metadata = {}
        for elem in tree.xpath("//dc:*", namespaces=ns):
            key = re.compile("\{.+?\}(.+?)$").findall(elem.tag)[0]
            value = elem.text
            self._metadata[key] = value
            
        # get cover image
        cover_images = tree.xpath("//pkg:item[@properties='cover-image']", namespaces=ns)
        all_images = tree.xpath("//pkg:item[@media-type='image/jpeg']", namespaces=ns)
        if cover_images: cover_image = os.path.join(os.path.dirname(self.opf_path), cover_images[0].get('href').replace("/", os.sep))
        elif all_images: cover_image = os.path.join(os.path.dirname(self.opf_path), all_images[0].get('href').replace("/", os.sep))
        else: cover_image = ""
        self.cover_image = cover_image
            
        
    def get_metadata(self, key):
        if not self._metadata:
            self._get_metadata()
        
        if key in self._metadata:
            return self._metadata[key]
        else:
            return None
        
    def get_cover_image(self):
        if not self._metadata:
            self._get_metadata()
            
        return self.cover_image
    
    
if __name__ == "__main__":
    import sys
    Epub(sys.argv[1], "./cache")
    