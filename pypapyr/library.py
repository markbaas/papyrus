from PyQt4 import QtCore, QtGui

class IconItem(QtGui.QListWidgetItem):
    
    def __init__(self, parent=None):
        super(IconItem, self).__init__(parent)
        
    def getPath(self):
        return self.data(QtCore.Qt.UserRole + 3)

class PapyrusLibraryWidget(QtGui.QListWidget):
    mouseItem = None
    buttonClicked = None
    contextMenu= None
   
    def __init__(self, parent=None):
        super(PapyrusLibraryWidget, self).__init__(parent)
        
        #self.delegate = PapyrusItemDelegate()
        #self.setItemDelegate(self.delegate)
        
        self.setContextMenuPolicy( QtCore.Qt.CustomContextMenu )
        self.customContextMenuRequested.connect(self.showContextMenu)

        
    def addThumb(self, thumb, path):

        # Set item
        item = IconItem()
        
        # Set thumb
        item.setData(QtCore.Qt.DecorationRole, QtGui.QPixmap(thumb).scaled(100, 150, QtCore.Qt.KeepAspectRatio))

        # Set path
        item.setData(QtCore.Qt.UserRole + 3, path)

        # Add to iconview
        self.addItem(item)

        
    def removeThumb(self, position):
        item = self.takeItem(position)
        #~ item.destroy()
        del item
        
    def resizeEvent(self, event):
        # Resort items
        self.sortItems()
        
        super(PapyrusLibraryWidget, self).resizeEvent(event)
        
    def _clear(self):
        # Clear function to clean itself and the delegate
        self.clear()
        
    def currentSelection(self):
        # Get selection, note: path names are in UserRole + 3
        return [unicode(x.data(QtCore.Qt.UserRole + 3)) for x in self.selectedIndexes()]
        
    def showContextMenu(self, point):
        # Show menu
        if self.contextMenu:
            self.contextMenu.exec_( self.mapToGlobal(point) )
        
    def setContextMenu(self, menu):
        self.contextMenu = menu 
