
import sys

from PyQt4 import QtCore, QtGui
from pypapyr.main import MainWindow

if __name__ == "__main__":
    # Setup app
    app = QtGui.QApplication(sys.argv)
    
    main = MainWindow()
    main.show()
        

    sys.exit(app.exec_()) 
