var pathsReadium = {
    jquery: 'jquery-1.9.1',
    underscore: 'underscore-1.4.4',
    backbone: 'backbone-0.9.10',
    bootstrap: 'bootstrap.min',
    Readium: 'Readium'
};

require.config({
    baseUrl: './lib/',
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: 'bootstrap'
        }
    },
    paths: pathsReadium
});

RJSApp = {};

require(['jquery', 'underscore', 'bootstrap', 'Readium'],
    function ($, _, bootstrap, Readium) {

    RJSApp.getInputValue = function (inputId) {
        return $("#" + inputId).val();
    };

    RJSApp.setModuleContainerHeight = function () {
        $("#epub-reader-container").css({ "height" : $(window).height() * 0.95 + "px", "margin-top": "0px" });
    };

    // This function will retrieve a package document and load an EPUB
    RJSApp.loadAndRenderEpub = function (packageDocumentURL) {

        // Set a fixed height for the epub viewer container
        RJSApp.setModuleContainerHeight();

        var that = this;
        var jsLibDir = './lib/';
        var elementToBindReaderTo = $("#epub-reader-container");
        $(elementToBindReaderTo).html("");

        // Clear the viewer, if it has been defined -> to load a new epub
        RJSApp.epubViewer = undefined;

        RJSApp.readium = new Readium(elementToBindReaderTo, packageDocumentURL, jsLibDir, function (epubViewer) {
            RJSApp.epubViewer = epubViewer;
            RJSApp.epubViewer.openBook();
            // Change layout stuff
            RJSApp.epubViewer.updateSettings({ "columnGap" : 20});
        });


        // These are application specific handlers that wire the HTML to the SimpleReadiumJs module API

        // Set handlers for click events
        $("#previous-page-btn").unbind("click");
        $("#previous-page-btn").on("click", function () {
            RJSApp.epubViewer.openPageLeft();
        });

        $("#next-page-btn").unbind("click");
        $("#next-page-btn").on("click", function () {
            RJSApp.epubViewer.openPageRight();
        });

        $("#toggle-synthetic-btn").unbind("click");
        $("#toggle-synthetic-btn").on("click", function () {

            if (RJSApp.currLayoutIsSynthetic) {
                RJSApp.epubViewer.updateSettings({ "isSyntheticSpread" : false });
                RJSApp.currLayoutIsSynthetic = false;
                $("#toggle-synthetic-btn").removeClass('synthetic-page-ico');
                $("#toggle-synthetic-btn").addClass('single-page-ico');
            }
            else {
                RJSApp.epubViewer.updateSettings({ "isSyntheticSpread" : true });
                RJSApp.currLayoutIsSynthetic = true;
                $("#toggle-synthetic-btn").removeClass('single-page-ico');
                $("#toggle-synthetic-btn").addClass('synthetic-page-ico');
            }
        });
    };
    
    RJSApp.loadBook = function(url){
        RJSApp.currLayoutIsSynthetic = true;
        RJSApp.loadAndRenderEpub(url);
    }

    // When the document is ready, choose an EPUB to show.
    $(document).ready(function () {
        
        //loadBook("../cache/burn_down_the_night.epub/OEBPS/content.opf");
        //RJSApp.currLayoutIsSynthetic = true;
        //RJSApp.loadAndRenderEpub("../cache/burn_down_the_night.epub/OEBPS/content.opf");
    });
    

});


